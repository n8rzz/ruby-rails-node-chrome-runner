set -ex

PROJECT_BASE=registry.gitlab.com
USERNAME=n8rzz
IMAGE=ruby-rails-node-chrome-runner

docker build -t $PROJECT_BASE/$USERNAME/$IMAGE:latest .
