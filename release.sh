set -ex

USERNAME=n8rzz
IMAGE=ruby-rails-node-chrome-runner

git pull

version=`cat VERSION`
echo "version: $version"

# ./build.sh

git add -A
git status

git commit -m "release version $version"
git tag -a "$version" -m "version $version"
git push
git push --tags
